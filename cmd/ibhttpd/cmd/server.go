/*
Copyright © 2021 Mike Dotson <mike.dotson@itsbroke.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"context"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/itsbroke/ibhttpd/server"
	"gitlab.com/itsbroke/ibhttpd/state"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Starts an IBhttpd server instance",
	Long: `IBhttpd is a webserver backed by a state machine 
	that is used for testing http client code.`,

	Run: func(cmd *cobra.Command, args []string) {
		log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

		ip, err := cmd.Flags().GetString("server")
		if err != nil {
			log.Fatalf("invalid flag server: %v", err)
		}

		port, err := cmd.Flags().GetInt("port")
		if err != nil {
			log.Fatalf("invalid flag type: %v", err)
		}

		statefile, err := cmd.Flags().GetString("statefile")
		if err != nil {
			log.Fatalf("invalid flag statefile: %v", err)
		}

		name, err := cmd.Flags().GetString("name")
		if err != nil {
			log.Fatalf("invalid flag name: %v", err)
		}

		quit, err := cmd.Flags().GetString("quit")
		if err != nil {
			log.Fatalf("invalid flag name: %v", err)
		}

		stateFile, err := os.Open(statefile)
		if err != nil {
			log.Fatalf("failed to open state file: %v", err)
		}

		path := filepath.Dir(statefile)
		log.Printf("path: %s", path)
		engine, err := state.New(name, stateFile, path)
		if err != nil {
			log.Fatalf("failure with state file: %s %v", stateFile.Name(), err)
			os.Exit(1)
		}

		addr, srv, err := server.ServerFunc(context.Background(), ip, port, *engine, quit)
		if err != nil {
			log.Fatalf("error starting server: %v", err)
		}
		log.Printf("server running on: %s", addr.String())
		log.Printf("%v", srv())
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.httpd.yaml)")

	serverCmd.Flags().StringP("server", "s", "127.0.0.1", "server ip address")
	serverCmd.Flags().IntP("port", "p", 0, "server port")
	serverCmd.Flags().StringP("statefile", "f", "", "location of the state file")
	serverCmd.Flags().StringP("name", "n", "ibhttpd", "name of the test engine")
	serverCmd.Flags().StringP("quit", "q", "/QUIT", "path that will trigger a server shutdown")
	serverCmd.MarkFlagRequired("statefile")
}
