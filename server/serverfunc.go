package server

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"time"

	"gitlab.com/itsbroke/ibhttpd/state"
)

// ServerFunc returns the network address and the server function.
func ServerFunc(ctx context.Context, ip string, port int, engine state.Engine, quit string) (net.Addr, func() error, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", ip, port))
	if err != nil {
		return nil, nil, err
	}

	server := &http.Server{
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 3600 * time.Second,
		ReadTimeout:  3600 * time.Second,
		IdleTimeout:  3600 * time.Second,
	}

	http.HandleFunc(quit, shutdown(server))
	http.HandleFunc("/", handleStates(engine))

	return listener.Addr(), func() error {
		return server.Serve(listener)
	}, nil
}

func shutdown(server *http.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5)*time.Second)
		w.WriteHeader(200)
		w.Write([]byte("server shutting down"))
		r.Body.Close()
		defer cancel()
		go func() {
			// give the server enough time to send out the final shutdown message
			time.Sleep(1 * time.Second)
			server.Shutdown(ctx)
		}()
	}
}

func handleStates(e state.Engine) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v", r.RequestURI)
		response, err := e.GetResponse(r.RequestURI, r.Method)
		if err != nil {
			log.Printf("error with response: %v", err)
			return
		}

		w.WriteHeader(response.Status)
		w.Header().Set("Content-Type", response.ContentType)
		body, err := io.ReadAll(*response.Body)
		if err != nil {
			log.Printf("error reading response: %v", err)
		}
		w.Write(body)
		(*response.Body).Close()
	}
}
