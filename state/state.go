package state

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/ghodss/yaml"
)

// PathType contains what the type of path is.
type PathType int

const (
	// EXACT specifies path should be exact match
	EXACT PathType = iota
	// GLOB specifies path should be string.Match glob expression
	GLOB
	// REGEX specifies we're going full bore on pattern matching
	REGEX

	// STATUS is the default status returned when no status is specified.
	STATUS = http.StatusNotFound
	// CONTENTTYPE is the default content type returned when no content type is specified.
	CONTENTTYPE = "text/plain"
)

var (
	// ErrUnmarshal is thrown when there is an issue with json marshaling
	ErrUnmarshal = errors.New("unable to json unmarshal content")

	// ErrMarshal is thrown when there is an issue with json unmarshaling
	ErrMarshal = errors.New("unable to json marshal content")

	// ErrYAMLConvert is thrown when there is an error converting from YAML
	// to JSON
	ErrYAMLConvert = errors.New("error with yaml to json conversion")
)

// Response contains the http response for the given state request.
type Response struct {
	ContentType string
	Body        *io.ReadCloser
	Status      int
}

// defaults is the default response if no state path is found
type defaults struct {
	Status      *int    `json:",omitempty"`
	Body        *string `json:"Body,omitempty"`
	File        *string `json:"File,omitempty"`
	ContentType *string `json:"Content-Type"`
}

// response defines the internal response for the path request.
type response struct {
	Status      *int    `json:",omitempty"`
	Body        *string `json:"Body,omitempty"`
	File        *string `json:"File"`
	ContentType *string `json:"Content-Type"`
}

// endpoint defines the endpoint for the state
type endpoint struct {
	URLPath   string   `json:"URL Path"`
	Methods   []string `json:"Method"`
	PathType  PathType `json:"Path Type"`
	NextState *string  `json:"Next State"`
	Response  response
}

// state defines the response based on the state.
type state struct {
	Name      string
	Endpoints []endpoint `json:",omitempty"`
	Default   defaults
}

// engine contains the engine logic for the http server.
type engine struct {
	mu      sync.Mutex
	name    string
	Default defaults
	States  []state
	paths   []string
	index   int
}

// Engine returns the engine state machine.
type Engine struct {
	engine *engine
}

// New returns a new state engine method.
func New(name string, f io.Reader, paths ...string) (*Engine, error) {
	// var s map[string]interface{}
	E := Engine{}
	E.engine = &engine{}

	s := engine{}
	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	jsonData, err := yaml.YAMLToJSON(data)
	// err = yaml.Unmarshal(data, &s)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrYAMLConvert, err)
	}

	err = json.Unmarshal(jsonData, &s)
	if err != nil {
		return nil, err
	}

	// log.Printf("Engine: %+v\n", s)
	// Validate config
	E.engine = &s
	E.engine.name = name
	E.engine.paths = append(E.engine.paths, paths...)
	return &E, nil
}

func (e *Engine) GetResponse(url string, method string) (*Response, error) {
	return e.engine.getResponse(url, method)
}

func (e *engine) getResponse(url string, method string) (*Response, error) {
	var r Response
	var fh io.ReadCloser = nil
	var err error
	defaults := true
	r.Status = STATUS
	r.ContentType = CONTENTTYPE

	for _, endpoint := range e.States[e.index].Endpoints {
		if url == endpoint.URLPath && contains(endpoint.Methods, method) {
			defaults = false
			if endpoint.Response.ContentType != nil {
				r.ContentType = *endpoint.Response.ContentType
			} else if e.States[e.index].Default.ContentType != nil {
				r.ContentType = *e.States[e.index].Default.ContentType
			} else if e.Default.ContentType != nil {
				r.ContentType = *e.Default.ContentType
			}

			r.Status = STATUS
			if endpoint.Response.Status != nil {
				r.Status = *endpoint.Response.Status
			} else if e.States[e.index].Default.Status != nil {
				r.Status = *e.States[e.index].Default.Status
			} else if e.Default.Status != nil {
				r.Status = *e.Default.Status
			}

			if endpoint.Response.File != nil {
				fh, err = openResponseFile(*endpoint.Response.File, e.paths...)
				if err != nil {
					return nil, err
				}
			} else if endpoint.Response.Body != nil {
				fh = io.NopCloser(strings.NewReader(*endpoint.Response.Body))
			} else if e.States[e.index].Default.File != nil {
				fh, err = os.Open(*e.States[e.index].Default.File)
				if err != nil {
					return nil, err
				}
			} else if e.States[e.index].Default.Body != nil {
				fh = io.NopCloser(strings.NewReader(*e.States[e.index].Default.Body))
			} else if e.Default.File != nil {
				fh, err = openResponseFile(*e.Default.File, e.paths...)
				if err != nil {
					return nil, err
				}
			} else if e.Default.Body != nil {
				fh = io.NopCloser(strings.NewReader(*e.Default.Body))
			}
			r.Body = &fh

			if endpoint.NextState != nil {
				for i := range e.States {
					if e.States[i].Name == *endpoint.NextState {
						e.SetIndex(i)
						break
					}
				}
			}
			break
		}
	}

	if defaults {
		// need to check the state defaults first...
		if e.States[e.index].Default.Status != nil {
			r.Status = *e.States[e.index].Default.Status
		} else if e.Default.Status != nil {
			r.Status = *e.Default.Status
		}

		if e.States[e.index].Default.ContentType != nil {
			r.ContentType = *e.States[e.index].Default.ContentType
		} else if e.Default.ContentType != nil {
			r.ContentType = *e.Default.ContentType
		}

		if e.States[e.index].Default.File != nil {
			fh, err = os.Open(*e.States[e.index].Default.File)
			if err != nil {
				return nil, err
			}
		} else if e.Default.File != nil {
			fh, err = os.Open(*e.Default.File)
			if err != nil {
				return nil, err
			}
		} else if e.States[e.index].Default.Body != nil {
			fh = io.NopCloser(strings.NewReader(*e.States[e.index].Default.Body))
		} else if e.Default.Body != nil {
			fh = io.NopCloser(strings.NewReader(*e.Default.Body))
		}
		r.Body = &fh
	}

	return &r, nil
}

func contains(methods []string, method string) bool {
	if methods == nil {
		return true
	}

	for _, m := range methods {
		if strings.EqualFold(m, method) {
			return true
		}
	}
	return false
}

func (e *engine) SetIndex(i int) {
	e.mu.Lock()
	e.index = i
	e.mu.Unlock()
}

func openResponseFile(file string, paths ...string) (io.ReadCloser, error) {
	// check current path level first
	if _, err := os.Stat(file); err == nil {
		return os.Open(file)
	}

	// check paths in order, returning firt available file
	for _, p := range paths {
		f := filepath.Join(p, file)
		if _, err := os.Stat(f); err == nil {
			return os.Open(f)
		}
	}
	return nil, fmt.Errorf("file not found: %s", file)
}
