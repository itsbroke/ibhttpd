package state

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var update bool

func TestMain(m *testing.M) {
	flag.BoolVar(&update, "update", false, "update golden file")
	flag.Parse()

	code := m.Run()
	os.Exit(code)
}

func TestNew(t *testing.T) {

	var tests = map[string]struct {
		config string
		name   string
		valid  bool
		engine Engine
		err    error
		paths  []string
	}{
		"Valid Config": {
			name:   "valid test",
			config: "valid_config.yaml",
			valid:  true,
			err:    nil,
		},
		"Invalid Config": {
			name:   "invalid test",
			config: "invalid_config.yaml",
			valid:  true,
			err:    ErrYAMLConvert,
		},
		"Valid JSON": {
			name:   "valid test",
			config: "valid_config.json",
			valid:  true,
			err:    nil,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			filePath := filepath.Join("testdata", tc.config)
			file, err := os.Open(filePath)
			if err != nil {
				t.Fatalf("test config file not found: %s, %v", filePath, err)
			}
			_, err = New(tc.name, file, tc.paths...)
			if err != nil && !errors.Is(err, tc.err) {
				t.Fatalf("engine failure: expected: %v; got: %v", tc.err, err)
			}
		})
	}
}

func TestState(t *testing.T) {

	var tests = map[string]struct {
		config      string
		valid       bool
		engine      Engine
		err         error
		get         string
		method      string
		contentType string
		results     string
		paths       []string
		status      int
	}{
		"Valid Test": {
			config:      "validengine.yaml",
			valid:       true,
			err:         nil,
			get:         "/goodbye",
			status:      200,
			contentType: "text/html",
			results:     "valid_test.golden",
			paths:       []string{"testdata"},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			filePath := filepath.Join("testdata", tc.config)
			file, err := os.Open(filePath)
			if err != nil {
				t.Fatalf("test config file not found: %s, %v", filePath, err)
			}

			engine, err := New("valid test", file, tc.paths...)
			file.Close()
			if err != nil && err != tc.err {
				t.Fatalf("engine failure: expected: %v; got: %v", tc.err, err)
			}

			resp, err := engine.GetResponse(tc.get, tc.method)
			if err != nil && !errors.Is(err, tc.err) {
				t.Errorf("error code does not match: %v %v", err, tc.err)
			}

			contents, err := io.ReadAll(*resp.Body)
			if err != nil {
				t.Errorf("unable to read response body: %v", err)
			}
			goldenFile := filepath.Join("testdata", filepath.FromSlash(tc.results))
			expected, err := ProcessGoldenFile(goldenFile, &contents, update)
			if err != nil {
				t.Errorf("unable to proecess golden file: %v", err)
			}
			if !cmp.Equal(*expected, contents) {
				t.Errorf("response contents do not match: %s, %s", *expected, contents)
			}
			if tc.status != resp.Status {
				t.Errorf("response status does not match: %d, %d", tc.status, resp.Status)
			}
			if tc.contentType != resp.ContentType {
				t.Errorf("response content type does not match: %s, %s", tc.contentType, resp.ContentType)
			}
		})
	}
}

// ProcessGoldenFile retrieves the provided golden file.  ProcessGoldenFile
// also checks the `update` flag to see if the golden file should be updated first.
func ProcessGoldenFile(goldenFile string, results *[]byte, update bool) (*[]byte, error) {
	if update {
		if err := os.WriteFile(goldenFile, *results, 0644); err != nil {
			return nil, fmt.Errorf("failed to update golden file: %w", err)
		}
	}

	expected, err := os.ReadFile(goldenFile)
	if err != nil {
		return nil, fmt.Errorf("failed reading .golden: %w", err)
	}

	return &expected, nil
}
